// Fill out your copyright notice in the Description page of Project Settings.

#include "Indestructible.h"


// Sets default values
AIndestructible::AIndestructible()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AIndestructible::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIndestructible::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

