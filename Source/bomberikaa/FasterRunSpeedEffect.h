// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Effect.h"
#include "FasterRunSpeedEffect.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERIKAA_API AFasterRunSpeedEffect : public AEffect
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
	float SpeedIncrease = 100;

	virtual void OnApplied_Implementation() override;
	virtual void OnEffectEnded_Implementation() override;
	
	
};
