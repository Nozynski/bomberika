// Fill out your copyright notice in the Description page of Project Settings.

#include "IncreasedRangeEffect.h"
#include "bomberikaaCharacter.h"



void AIncreasedRangeEffect::OnApplied_Implementation()
{
	AffectedActor->BombRange += RangeIncrease;
}

void AIncreasedRangeEffect::OnEffectEnded_Implementation()
{

}
