// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBombExplodedEvent, AActor*, bomb);

UCLASS()
class BOMBERIKAA_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	

	ABomb();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	float TimeToLive = 3.f;
	UPROPERTY(BlueprintReadWrite, Category = Bomb)
	class AbomberikaaCharacter* BombOwner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	FOnBombExplodedEvent OnBombExplodedDelegate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	TSubclassOf<AActor> ParticleClass;

	UPROPERTY()
	bool bAlreadyExploded = false;

	UFUNCTION(BlueprintCallable, Category = Bomb)
	void Explode();
	UFUNCTION(BlueprintNativeEvent, Category = Bomb)
	void OnExploded();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = Bomb)
	float SpawnTime;

public:	

	virtual void Tick(float DeltaTime) override;
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	
};
