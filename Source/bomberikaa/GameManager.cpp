// Fill out your copyright notice in the Description page of Project Settings.

#include "GameManager.h"
#include "Bomb.h"
#include "bomberikaaCharacter.h"
#include "EngineUtils.h"


AGameManager::AGameManager()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AGameManager::BeginPlay()
{
	Super::BeginPlay();
	GenerateBoard();
}

void AGameManager::GenerateBoard()
{
	GenerateOuterBoard();

	int width = GameBoardHeight / WorldToTileDivider;
	int height = GameBoardWidth / WorldToTileDivider;

	for (int i = 0; i < width; ++i)
	{
		for (int j = 0; j < height; ++j)
		{
			FTile tile(i, j);

			bool isTooCloseToCorner = ((i == 0 && (j < 3 || j > height - 4)) || (i == width - 1 && (j < 3 || j > height - 4)));
			bool shouldBeIndestructible = (i % 2 == 1 && j % 2 == 1) && i > 0 && i < width - 1 && j > 0 && j < height - 1;

			if ( shouldBeIndestructible )
			{
				int randomIndex = FMath::RandHelper(IndestructibleClasses.Num());
				SpawnOnTile(tile, IndestructibleClasses[randomIndex]);
			}
			else if (!isTooCloseToCorner && AGameManager::Roll(DestructibleProbability))
			{
				int randomIndex = FMath::RandHelper(DestructibleClasses.Num());
				SpawnOnTile(tile, DestructibleClasses[randomIndex]);
			}
		}
	}
}

void AGameManager::GenerateOuterBoard()
{
	int width = GameBoardHeight / WorldToTileDivider;
	int height = GameBoardWidth / WorldToTileDivider;

	for (int i = -1; i <= width; ++i)
	{
		SpawnOnTile(FTile(i, -1), IndestructibleClasses[0]);
		SpawnOnTile(FTile(i, height), IndestructibleClasses[0]);
	}
	for (int j = 0; j < height; ++j)
	{
		SpawnOnTile(FTile(-1, j), IndestructibleClasses[0]);
		SpawnOnTile(FTile(width, j), IndestructibleClasses[0]);
	}
}

bool AGameManager::Roll(float probability)
{
	float roll = FMath::RandRange(0.f, 1.f);
	return roll <= probability;
}

AActor * AGameManager::SpawnOnTile(const FTile & tile, const TSubclassOf<AActor>& classToSpawn)
{
	auto location = tile.ToVector() * WorldToTileDivider;
	auto spawned = GetWorld()->SpawnActor(classToSpawn, &location, &FRotator::ZeroRotator);
	BoardObjects.Add(tile, spawned);
	AddToBoard(spawned);
	return spawned;
}

AActor* AGameManager::GetObjectOnTile(const FTile& tile) const
{
	return BoardObjects.Contains(tile) ? BoardObjects[tile] : nullptr;
}

bool AGameManager::IsTileEmpty(const FTile& tile) const
{
	return !BoardObjects.Contains(tile) || BoardObjects[tile] == nullptr;
}

void AGameManager::AddToBoard(AActor* actor)
{
	auto bomb = Cast<ABomb>(actor);
	if (IsValid(bomb))
	{
		bomb->OnBombExplodedDelegate.AddDynamic(this, &AGameManager::OnBombExploded);
	}
	else
	{
		auto crate = Cast<ADestructible>(actor);
		if (IsValid(crate))
		{
			crate->OnBrokenEvent.AddDynamic(this, &AGameManager::OnDestructibleBroken);
			crate->WorldToTileDivider = WorldToTileDivider;
		}
	}
}

void AGameManager::OnBombExploded(AActor* bomb)
{
	auto actualBomb = Cast<ABomb>(bomb);

	FTile t;
	t = bomb->GetActorLocation() / WorldToTileDivider;
	if (BoardObjects.Contains(t))
	{
		StartExplosion(t, actualBomb->BombOwner->BombRange, actualBomb);
	}
}

void AGameManager::StartExplosion(const FTile& start, int range, ABomb* bomb)
{
	ApplyExplosionOnTile(start, bomb);

	for (int i = 1; i <= range; i++)
	{
		if (!ApplyExplosionOnTile(start + FTile(i, 0), bomb))
		{
			break;
		}
		
	}
	for (int i = 1; i <= range; i++)
	{
		if(!ApplyExplosionOnTile(start + FTile(-i, 0), bomb))
			break;
	}
	for (int i = 1; i <= range; i++)
	{
		if(!ApplyExplosionOnTile(start + FTile(0, i), bomb))
			break;
	}
	for (int i = 1; i <= range; i++)
	{
		if(!ApplyExplosionOnTile(start + FTile(0, -i), bomb))
			break;
	}
}

bool AGameManager::ApplyExplosionOnTile(const FTile& tile, ABomb* bomb)
{
	if (!IsValid(bomb))
		return false;

	for (TActorIterator<AbomberikaaCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->Tile == tile) 
		{
			ActorItr->TakeDamage(1.f, FPointDamageEvent(), bomb->BombOwner->GetInstigatorController(), bomb->BombOwner);
		}
	}

	if (!BoardObjects.Contains(tile))
	{
		auto location = tile.ToVector() * WorldToTileDivider;
		auto particle = GetWorld()->SpawnActor(bomb->ParticleClass, &location, &FRotator::ZeroRotator);
		return true;
	}
		

	auto objectOnTile = BoardObjects[tile];

	if (IsValid(objectOnTile) && IsValid(bomb->BombOwner) && bomb != objectOnTile)
	{
		auto ind = Cast<AIndestructible>(objectOnTile);
		if (IsValid(ind))
		{
			return false;
		}
		else
		{
			objectOnTile->TakeDamage(1.f, FPointDamageEvent(), bomb->BombOwner->GetInstigatorController(), bomb->BombOwner);
		}
	}

	auto location = tile.ToVector() * WorldToTileDivider;
	auto particle = GetWorld()->SpawnActor(bomb->ParticleClass, &location, &FRotator::ZeroRotator);
	return true;
}

void AGameManager::OnDestructibleBroken(const FTile& tile)
{
	if (AGameManager::Roll(0.3f))
	{
		int randomIndex = FMath::RandHelper(PickupClasses.Num());
		SpawnOnTile(tile, PickupClasses[randomIndex]);
	}
}

void AGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

