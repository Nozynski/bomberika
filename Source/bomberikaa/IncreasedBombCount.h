// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Effect.h"
#include "IncreasedBombCount.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERIKAA_API AIncreasedBombCount : public AEffect
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
	int LimitIncrease = 1;

	virtual void OnApplied_Implementation() override;
	virtual void OnEffectEnded_Implementation() override;
	
	
};
