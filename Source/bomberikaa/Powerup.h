// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Powerup.generated.h"

UCLASS()
class BOMBERIKAA_API APowerup : public AActor
{
	GENERATED_BODY()
	
public:

	virtual void NotifyActorBeginOverlap(AActor* other) override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> EffectToApply;

public:
	UFUNCTION(BlueprintCallable, Category = Powerups)
	virtual void Pickup(class AbomberikaaCharacter* player);
	UFUNCTION(BlueprintNativeEvent, Category = Powerups)
	void OnPickup(class AbomberikaaCharacter* player);
	
	
};
