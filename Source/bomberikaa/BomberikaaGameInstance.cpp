// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberikaaGameInstance.h"




void UBomberikaaGameInstance::PlayersScored(int firstPlayerScore, int secondPlayerScore)
{
	PlayerWins.SetNum(2);
	PlayerWins[0] += firstPlayerScore;
	PlayerWins[1] += secondPlayerScore;

	LastPlayerWins.SetNum(2);
	LastPlayerWins[0] = firstPlayerScore;
	LastPlayerWins[1] = secondPlayerScore;
}
