// Fill out your copyright notice in the Description page of Project Settings.

#include "Powerup.h"
#include "bomberikaaCharacter.h"
#include "Effect.h"

void APowerup::NotifyActorBeginOverlap(AActor* other)
{
	auto player = Cast<AbomberikaaCharacter>(other);
	if (IsValid(player))
	{
		Pickup(player);
	}
}

void APowerup::Pickup(AbomberikaaCharacter* player)
{
	OnPickup(player);
	FVector spawnLocation = player->GetActorLocation();
	AActor* spawnedEffect = GetWorld()->SpawnActor(EffectToApply, &spawnLocation, &FRotator::ZeroRotator);
	auto effect = Cast<AEffect>(spawnedEffect);
	if (IsValid(effect))
	{
		effect->ApplyEffect(player);
	}

	Destroy();
}

void APowerup::OnPickup_Implementation(AbomberikaaCharacter* player)
{

}

