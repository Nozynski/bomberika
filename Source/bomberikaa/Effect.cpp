// Fill out your copyright notice in the Description page of Project Settings.

#include "Effect.h"
#include "bomberikaaCharacter.h"

void AEffect::OnApplied_Implementation()
{

}

void AEffect::ApplyEffect(AbomberikaaCharacter* victim)
{
	AffectedActor = victim;
	OnApplied();
	GetWorldTimerManager().SetTimer(EffectTimerHandle, this, &AEffect::EndEffect, Duration, false);
}

void AEffect::EndEffect()
{
	OnEffectEnded();
	Destroy();
}

void AEffect::OnEffectEnded_Implementation()
{

}

