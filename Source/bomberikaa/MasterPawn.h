// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "bomberikaaCharacter.h"
#include "MasterPawn.generated.h"

UCLASS()
class BOMBERIKAA_API AMasterPawn : public APawn
{
	GENERATED_BODY()

public:

	AMasterPawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MasterPawn)
	TArray<AbomberikaaCharacter*> Players;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MasterPawn)
	float GameLength = 120.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MasterPawn)
	float CurrentGameTime = 0.f;

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	

	void MoveForward1(float Value);
	void MoveRight1(float Value);
	void PlaceBomb1();
	void MoveForward2(float Value);
	void MoveRight2(float Value);
	void PlaceBomb2();

	UFUNCTION(BlueprintCallable, Category = MasterPawn)
	void EndGame();
	UFUNCTION(BlueprintNativeEvent, Category = MasterPawn)
	void OnGameEnded();
};
