// Fill out your copyright notice in the Description page of Project Settings.

#include "Destructible.h"

ADestructible::ADestructible()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ADestructible::Break()
{
	if (OnBrokenEvent.IsBound())
	{
		FTile tile;
		tile = GetActorLocation() / WorldToTileDivider;
		OnBrokenEvent.Broadcast(tile);
	}
	OnBroken();
	Destroy();
}

void ADestructible::OnBroken_Implementation()
{

}

void ADestructible::BeginPlay()
{
	Super::BeginPlay();
	
}

void ADestructible::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ADestructible::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Break();
	return Damage;
}

