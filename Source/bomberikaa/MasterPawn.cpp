// Fill out your copyright notice in the Description page of Project Settings.

#include "MasterPawn.h"
#include "EngineUtils.h"
#include "MainCameraActor.h"
#include "BomberikaaGameInstance.h"


AMasterPawn::AMasterPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

bool _gameEnded = false;
float _gameStart = 0.f;

void AMasterPawn::BeginPlay()
{
	Super::BeginPlay();
	
	_gameEnded = false;
	_gameStart = GetWorld()->TimeSeconds;
	CurrentGameTime = 0.f;

	TActorIterator<AMainCameraActor> CameraIterator(GetWorld());
	if (CameraIterator)
	{
		auto playerController = Cast<APlayerController>(GetController());
		if (playerController)
		{
			playerController->SetViewTarget(*CameraIterator);
		}
	}
}


void AMasterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (_gameEnded)
		return;

	CurrentGameTime += DeltaTime;

	if (GetWorld()->TimeSeconds > _gameStart + GameLength)
	{
		auto instance = Cast<UBomberikaaGameInstance>(GetGameInstance());
		if (IsValid(instance))
		{
			_gameEnded = true;
			instance->PlayersScored(1, 1);
			EndGame();
			return;
		}
	}

	for(int i = 0; i < Players.Num(); ++i)
	{
		if (Players[i]->bIsDead)
		{
			_gameEnded = true;
			const int firstScore = i == 0 ? 1 : 0;
			const int secondScore = i == 1 ? 1 : 0;

			auto instance = Cast<UBomberikaaGameInstance>(GetGameInstance());
			if (IsValid(instance))
			{
				instance->PlayersScored(firstScore, secondScore);
				EndGame();
			}
		}
	}
}

void AMasterPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward1", this, &AMasterPawn::MoveForward1);
	PlayerInputComponent->BindAxis("MoveRight1", this, &AMasterPawn::MoveRight1);
	PlayerInputComponent->BindAction("PlaceBomb1", IE_Released, this, &AMasterPawn::PlaceBomb1);
	PlayerInputComponent->BindAxis("MoveForward2", this, &AMasterPawn::MoveForward2);
	PlayerInputComponent->BindAxis("MoveRight2", this, &AMasterPawn::MoveRight2);
	PlayerInputComponent->BindAction("PlaceBomb2", IE_Released, this, &AMasterPawn::PlaceBomb2);
}

void AMasterPawn::MoveForward1(float Value)
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 1)
	{
		Players[0]->MoveForward(Value);
	}
}

void AMasterPawn::MoveRight1(float Value)
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 1)
	{
		Players[0]->MoveRight(Value);
	}
}

void AMasterPawn::PlaceBomb1()
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 1)
	{
		Players[0]->PlaceBomb();
	}
}

void AMasterPawn::MoveForward2(float Value)
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 2)
	{
		Players[1]->MoveForward(Value);
	}
}

void AMasterPawn::MoveRight2(float Value)
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 2)
	{
		Players[1]->MoveRight(Value);
	}
}

void AMasterPawn::PlaceBomb2()
{
	if (_gameEnded)
		return;

	if (Players.Num() >= 2)
	{
		Players[1]->PlaceBomb();
	}
}

void AMasterPawn::EndGame()
{
	OnGameEnded();
}

void AMasterPawn::OnGameEnded_Implementation()
{

}

