// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomb.h"


ABomb::ABomb()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ABomb::Explode()
{
	if (bAlreadyExploded)
		return;

	bAlreadyExploded = true;

	if (OnBombExplodedDelegate.IsBound())
	{
		OnBombExplodedDelegate.Broadcast(this);
	}
	
	OnExploded();
	OnBombExplodedDelegate.Clear();
	Destroy();
}

void ABomb::OnExploded_Implementation()
{

}

void ABomb::BeginPlay()
{
	Super::BeginPlay();
	SpawnTime = GetWorld()->TimeSeconds;
}

void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GetWorld()->TimeSeconds > SpawnTime + TimeToLive)
	{
		Explode();
	}
}

float ABomb::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Explode();
	return Damage;
}

