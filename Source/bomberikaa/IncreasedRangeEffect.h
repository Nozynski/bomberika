// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Effect.h"
#include "IncreasedRangeEffect.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERIKAA_API AIncreasedRangeEffect : public AEffect
{
	GENERATED_BODY()
	
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
	int RangeIncrease = 1;

	virtual void OnApplied_Implementation() override;
	virtual void OnEffectEnded_Implementation() override;
	
};
