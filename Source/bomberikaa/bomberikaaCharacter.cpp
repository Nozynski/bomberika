// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "bomberikaaCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "MainCameraActor.h"
#include "EngineUtils.h"
#include "GameManager.h"
#include "Bomb.h"

//////////////////////////////////////////////////////////////////////////
// AbomberikaaCharacter

AbomberikaaCharacter::AbomberikaaCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bAllowTickOnDedicatedServer = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	/*bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;*/

	// Configure character movement
	//GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	//GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
}

void AbomberikaaCharacter::BeginPlay()
{
	Super::BeginPlay();

	TActorIterator<AGameManager> ManagerIterator(GetWorld());
	if (ManagerIterator)
	{
		GameManager = *ManagerIterator;
		WorldToTileDivider = ManagerIterator->WorldToTileDivider;
	}

	Tile = GetActorLocation();
	Tile /= WorldToTileDivider;
}

void AbomberikaaCharacter::Tick(float delta)
{
	Super::Tick(delta);

	for (int i = ActiveBombs.Num() - 1; i >= 0; --i)
	{
		if (!IsValid(ActiveBombs[i]))
		{
			ActiveBombs.RemoveAt(i);
		}
	}

	Tile = GetActorLocation();
	Tile /= WorldToTileDivider;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AbomberikaaCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	/*PlayerInputComponent->BindAxis("MoveForward", this, &AbomberikaaCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AbomberikaaCharacter::MoveRight);
	PlayerInputComponent->BindAction("PlaceBomb", IE_Released, this, &AbomberikaaCharacter::PlaceBomb);*/
}

const FRotator _up = FRotator::MakeFromEuler(FVector(0, 0, 0));
const FRotator _down = FRotator::MakeFromEuler(FVector(0, 0, 180));

void AbomberikaaCharacter::MoveForward(float Value)
{
	if (Value > 0.0f)
	{
		SetActorRotation(_up);
	}
	else if(Value < 0.0f)
	{
		SetActorRotation(_down);
	}

	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), FMath::Abs(Value));
	}
}

const FRotator _right = FRotator::MakeFromEuler(FVector(0, 0, 90));
const FRotator _left = FRotator::MakeFromEuler(FVector(0, 0, 270));

void AbomberikaaCharacter::MoveRight(float Value)
{
	if (Value > 0.0f)
	{
		SetActorRotation(_right);
	}
	else if (Value < 0.0f)
	{
		SetActorRotation(_left);
	}

	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), FMath::Abs(Value));
	}
}

void AbomberikaaCharacter::PlaceBomb()
{
	if (ActiveBombs.Num() >= BombLimit)
		return;

	if (IsValid(GameManager) && IsValid(BombClass) && GameManager->IsTileEmpty(Tile))
	{
		auto bomb = GameManager->SpawnOnTile(Tile, BombClass);
		ActiveBombs.Add(bomb);
		auto actualBomb = Cast<ABomb>(bomb);
		actualBomb->BombOwner = this;
	}
}

float AbomberikaaCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	bIsDead = true;
	return Damage;
}
