// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "bomberikaa.h"
#include "Destructible.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDestructibleBroken, const FTile&, tile);

UCLASS()
class BOMBERIKAA_API ADestructible : public AActor
{
	GENERATED_BODY()
	
public:	

	ADestructible();

	UPROPERTY(BlueprintReadWrite, Category = Destructible)
	FOnDestructibleBroken OnBrokenEvent;
	UPROPERTY()
	float WorldToTileDivider;

	UFUNCTION(BlueprintCallable, Category = Destructible)
	void Break();
	UFUNCTION(BlueprintNativeEvent, Category = Destructible)
	void OnBroken();

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	
};
