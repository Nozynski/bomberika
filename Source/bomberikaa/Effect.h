// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Effect.generated.h"

UCLASS()
class BOMBERIKAA_API AEffect : public AActor
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Duration = 5.f;
	UPROPERTY()
	class AbomberikaaCharacter* AffectedActor;

public:

	UFUNCTION(BlueprintCallable, Category = Effects)
	void ApplyEffect(class AbomberikaaCharacter* victim);
	/*
	* Native event for easy overriding in blueprints,
	* e.g. when we want to play a sound or particle on applying effect
	* or adding other custom logic
	*/
	UFUNCTION(BlueprintNativeEvent, Category = Effects)
	void OnApplied();
	UFUNCTION(BlueprintCallable, Category = Effects)
	void EndEffect();
	UFUNCTION(BlueprintNativeEvent, Category = Effects)
	void OnEffectEnded();

private:
	FTimerHandle EffectTimerHandle;
	
};
