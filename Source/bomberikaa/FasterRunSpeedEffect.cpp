// Fill out your copyright notice in the Description page of Project Settings.

#include "FasterRunSpeedEffect.h"
#include "bomberikaaCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"



void AFasterRunSpeedEffect::OnApplied_Implementation()
{
	AffectedActor->GetCharacterMovement()->MaxWalkSpeed += SpeedIncrease;
}

void AFasterRunSpeedEffect::OnEffectEnded_Implementation()
{

}
