// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "bomberikaa.generated.h"

USTRUCT(BlueprintType)
struct FTile
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tile)
	int32 x;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tile)
	int32 y;

	FTile()
		:x(0), y(0)
	{

	}

	FTile(int _x, int _y) 
		:x(_x), y(_y)
	{

	}

	FTile& operator =(const FTile& tile)
	{
		x = tile.x;
		y = tile.y;
		return *this;
	}

	FTile& operator =(const FVector& vec)
	{
		x = FMath::RoundToInt(vec.X);
		y = FMath::RoundToInt(vec.Y);
		return *this;
	}

	bool operator ==(const FTile& tile) const
	{
		return x == tile.x && y == tile.y;
	}

	FTile operator +(const FTile& tile) const
	{
		return FTile(x + tile.x, y + tile.y);
	}

	FTile operator -(const FTile& tile) const
	{
		return FTile(x - tile.x, y - tile.y);
	}

	FTile& operator +=(const FTile& tile)
	{
		x += tile.x;
		y += tile.y;
		return *this;
	}

	FTile& operator -=(const FTile& tile)
	{
		x -= tile.x;
		y -= tile.y;
		return *this;
	}

	FTile operator *(float multiplier)
	{
		return FTile(x * multiplier, y * multiplier);
	}

	FTile& operator *=(float multiplier)
	{
		x = x * multiplier;
		y = y * multiplier;
		return *this;
	}

	FTile operator /(float divider)
	{
		check(divider != 0.f);
		return FTile(x / divider, y / divider);
	}

	FTile& operator /=(float divider)
	{
		check(divider != 0.f);
		x = x / divider;
		y = y / divider;
		return *this;
	}

	FVector ToVector() const
	{
		return FVector(x, y, 0.f);
	}

	friend uint32 GetTypeHash(const FTile& t)
	{
		return t.x * 31 + t.y;
	}
};