// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Indestructible.h"
#include "Destructible.h"
#include "bomberikaa.h"
#include "GameManager.generated.h"

UCLASS()
class BOMBERIKAA_API AGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameManager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	float GameBoardHeight = 2000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	float GameBoardWidth = 2000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	float WorldToTileDivider = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	float IndestructibleProbability = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	float DestructibleProbability = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	TArray<TSubclassOf<AActor>> IndestructibleClasses;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	TArray<TSubclassOf<AActor>> DestructibleClasses;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameManager)
	TArray<TSubclassOf<AActor>> PickupClasses;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void GenerateBoard();
	UFUNCTION()
	void GenerateOuterBoard();
	UFUNCTION()
	static bool Roll(float probability);


	UPROPERTY(BlueprintReadWrite, Category = GameManager)
	TMap<FTile, AActor*> BoardObjects;


public:

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = GameManager)
	AActor* SpawnOnTile(const FTile& tile, const TSubclassOf<AActor>& classToSpawn);
	UFUNCTION(BlueprintCallable, Category = GameManager)
	AActor* GetObjectOnTile(const FTile& tile) const;
	UFUNCTION(BlueprintCallable, Category = GameManager)
	bool IsTileEmpty(const FTile& tile) const;
	UFUNCTION(BlueprintCallable, Category = GameManager)
	void AddToBoard(AActor* actor);
	UFUNCTION()
	void OnBombExploded(AActor* bomb);
	UFUNCTION(BlueprintCallable, Category = GameManager)
	void StartExplosion(const FTile& start, int range, class ABomb* bomb);
	UFUNCTION(BlueprintCallable, Category = GameManager)
	bool ApplyExplosionOnTile(const FTile& tile, class ABomb* bomb);
	UFUNCTION()
	void OnDestructibleBroken(const FTile& tile);
	
};
