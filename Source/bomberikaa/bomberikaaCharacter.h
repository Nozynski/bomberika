// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "bomberikaa.h"
#include "GameFramework/Character.h"
#include "bomberikaaCharacter.generated.h"

UCLASS(config=Game)
class AbomberikaaCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	AbomberikaaCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	FTile Tile;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	TSubclassOf<AActor> BombClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	int BombLimit = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	int BombRange = 1;

	UPROPERTY(BlueprintReadWrite, Category = Player)
	bool bIsDead = false;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void PlaceBomb();

	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:

	virtual void BeginPlay() override;
	virtual void Tick(float delta) override;

	UPROPERTY()
	class AGameManager* GameManager;
	UPROPERTY(BlueprintReadWrite, Category = Player)
	float WorldToTileDivider = 200.f;
	UPROPERTY(BlueprintReadWrite, Category = Player)
	TArray<AActor*> ActiveBombs;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface
};

