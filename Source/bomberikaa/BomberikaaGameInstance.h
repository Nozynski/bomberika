// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BomberikaaGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERIKAA_API UBomberikaaGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, Category = GameInstance)
	TArray<int> PlayerWins;

	UPROPERTY(BlueprintReadWrite, Category = GameInstance)
	TArray<int> LastPlayerWins;
	
	UFUNCTION(BlueprintCallable, Category = GameInstance)
	void PlayersScored(int firstPlayerScore, int secondPlayerScore);
	

};
