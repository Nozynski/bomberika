Time spent = 13,5 hours

The only thing outside of my own creations I've used is ThirdPerson template from UE

Controls: 

First player: wsad + left control
Second player: arrow keys + right control

Things I didn't have time to finish:
Camera panning (bonus)
remote controlled bomb pickup
draw when both players die from the same blast
AI (bonus)

Apart from finishing the things above if I was to continue working on this project I would definitely look into general game feel, since the controls don't feel that great, and placing of the bombs sometimes seems off, since the perspective of the player is pretty weird, and there is no proper visual/sound/feel feedback for explosions. And the UI was done as the last thing, so it's a bit rushed (and definitely ugly). I'd also think about more interesting pickups and maybe different game modes. I've actually worked on a similar project a while ago (my first published game - BRAWL). It was done in Unity but concepts stay.